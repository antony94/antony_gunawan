package data

import (
	"database/sql"
	"log"
	//import Driver for mysql
	_ "github.com/go-sql-driver/mysql"
)

const dbProtocol = "tcp"
const dbDriver = "mysql"
const dbHost = "bkagsbpv3-mysql.services.clever-cloud.com:3306"
const dbUsername = "uq0aqyyfnvr2xbnr"
const dbPassword = "QGXOOLr6VauNyqIJa5o"
const dbName = "bkagsbpv3"

//file for organizing function that related to data source

var db *sql.DB
var err error

func openDb() {
	dataSourceName := dbUsername + ":" + dbPassword + "@" + dbProtocol + "(" + dbHost + ")/" + dbName
	db, err = sql.Open(dbDriver, dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
}

//ExecuteQueryRow used for executing query string for method SELECT and return single row
func ExecuteQueryRow(sql string, args ...interface{}) *sql.Row {
	openDb()
	defer db.Close()
	row := db.QueryRow(sql)
	return row
}

//ExecuteQueryRows used for executing query string for method SELECT and return multiple row
func ExecuteQueryRows(sql string, args ...interface{}) *sql.Rows {
	openDb()
	defer db.Close()
	rows, err := db.Query(sql)
	if err != nil {
		log.Fatal(err)
		return nil
	}
	return rows
}

//ExecuteStatement used for executing query string for method INSERT, UPDATE, DELETE, etc
func ExecuteStatement(sql string, args ...interface{}) (int64, error) {
	openDb()
	defer db.Close()
	stmt, err := db.Prepare(sql)
	defer stmt.Close()
	if err != nil {
		log.Fatal(err)
	}
	r, err := stmt.Exec()
	n, err := r.RowsAffected()
	return n, err
}
