package main

import (
	"mux"
	"net/http"
)

func main() {
	mux := mux.CreateMux()
	http.ListenAndServe(":8080", mux)
}
