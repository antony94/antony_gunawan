package controller

import (
	"log"
	"model"
	"net/http"
	"strconv"
	"time"
	"util"
)

//Login handle logic for mux login API
func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		password := r.FormValue("password")
		googleID := r.FormValue("google_id")

		/*if password != "" {
			password = util.GenerateBcrypt(password)
		}*/

		requiredFields := map[string]string{
			"email": email,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else if !model.CheckUserAccountCredential(email, password, googleID) {
			res, err := util.CreateResponse(400, true, "Incorrect Account Credential", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			u, err := model.GetUserByEmail(email)
			if err != nil {
				log.Fatal(err)
			}
			token := util.GenerateHmacToken(u.Email + u.Name + time.Now().String())
			model.InsertSession(u.ID, token)
			data := struct {
				Token string `json:"token"`
			}{
				token,
			}
			res, err := util.CreateResponse(200, false, "Success Login", data)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}

//Register handle logic for mux register API
func Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodPost {
		email := r.FormValue("email")
		password := r.FormValue("password")
		googleID := r.FormValue("google_id")
		name := r.FormValue("name")
		address := r.FormValue("address")
		phone := r.FormValue("phone")

		requiredFields := map[string]string{
			"email": email,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else if model.IsEmailAlreadyRegistered(email) {
			res, err := util.CreateResponse(400, true, "Email already taken", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else if password == "" && googleID == "" {
			res, err := util.CreateResponse(400, true, "Password or Google ID needed", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			if password != "" {
				password = util.GenerateBcrypt(password)
			}
			model.InsertUser(email, password, googleID, name, address, phone)
			u, err := model.GetUserByCredential(email, password, googleID)
			if err != nil {
				log.Fatal(err)
			}
			token := util.GenerateHmacToken(u.Email + u.Name + time.Now().String())
			model.InsertSession(u.ID, token)
			data := struct {
				Token string `json:"token"`
			}{
				token,
			}
			res, err := util.CreateResponse(200, false, "Success Registering New User", data)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}

//Profile handle logic for mux profile API
func Profile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")
	if r.Method == http.MethodGet {
		token := r.FormValue("token")

		requiredFields := map[string]string{
			"token": token,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else if model.IsSessionExpired(token) {
			res, err := util.CreateResponse(400, true, "Session already expired. Re-login is needed", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			uid, err := model.GetUIDByToken(token)
			if err != nil {
				log.Fatal(err)
			}
			u, err := model.GetUserByID(strconv.Itoa(uid))
			if err != nil {
				log.Fatal(err)
			}
			data := struct {
				User *model.User `json:"user"`
			}{
				u,
			}
			res, err := util.CreateResponse(200, false, "Succeed getting user profile", data)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}

//ProfileUpdate handle logic for mux profile/update API
func ProfileUpdate(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")

	if r.Method == http.MethodPost {
		token := r.FormValue("token")
		name := r.FormValue("name")
		address := r.FormValue("address")
		phone := r.FormValue("phone")

		requiredFields := map[string]string{
			"token":   token,
			"name":    name,
			"address": address,
			"phone":   phone,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			uid, err := model.GetUIDByToken(token)
			if err != nil {
				log.Fatal(err)
			}
			model.UpdateUserByID(strconv.Itoa(uid), name, address, phone)
			res, err := util.CreateResponse(200, false, "Succeed updating user profile", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}

//ForgotPassword handle logic for mux forgot-password API
func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")

	if r.Method == http.MethodPost {
		email := r.FormValue("email")

		requiredFields := map[string]string{
			"email": email,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else if !model.IsEmailAlreadyRegistered(email) {
			res, err := util.CreateResponse(400, true, "Email not yet registered", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			u, err := model.GetUserByEmail(email)
			if err != nil {
				log.Fatal(err)
			}
			s := strconv.Itoa(u.ID) + time.Now().String()
			token := util.GenerateHmacToken(s)
			model.InsertForgotPassword(strconv.Itoa(u.ID), token, "0")
			url := "http://app-1e9a20c6-8114-4c1f-8180-eee66da530e3.cleverapps.io/reset-password?token=" + token
			err = util.SendEmail(email, "Reset Password Link", "Please follow this link below to reset your password\n"+url)
			res, err := util.CreateResponse(200, false, "Reset password link has been sent to your email", nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}

//ResetPassword handle logic for mux reset-password API
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
	w.Header().Set("Content-Type", "application/json")

	if r.Method == http.MethodPost {
		token := r.FormValue("token")
		password := r.FormValue("password")

		requiredFields := map[string]string{
			"token":    token,
			"password": password,
		}

		if msg, ok := util.CheckRequiredField(requiredFields); !ok {
			res, err := util.CreateResponse(400, true, msg, nil)
			if err != nil {
				log.Fatal(err)
			}
			w.Write(res)
		} else {
			uid, err := model.GetUIDByForgotPassToken(token)
			if err != nil {
				res, err := util.CreateResponse(400, true, "Token provided is invalid", nil)
				if err != nil {
					log.Fatal(err)
				}
				w.Write(res)
			} else {
				password = util.GenerateBcrypt(password)
				model.UpdateUserPasswordByID(uid, password)
				model.UpdateForgotPasswordStatusByToken(token, "1")
				res, err := util.CreateResponse(200, false, "Success updating your password", nil)
				if err != nil {
					log.Fatal(err)
				}
				w.Write(res)
			}
		}
	} else {
		res := util.CreateResponseNotAllowed()
		w.Write(res)
	}
}
