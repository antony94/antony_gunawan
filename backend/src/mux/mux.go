package mux

import (
	"controller"
	"net/http"
)

func CreateMux() *http.ServeMux {
	mux := http.NewServeMux()

	//handling our custom routes
	mux.HandleFunc("/api/login", controller.Login)
	mux.HandleFunc("/api/register", controller.Register)
	mux.HandleFunc("/api/profile", controller.Profile)
	mux.HandleFunc("/api/profile/update", controller.ProfileUpdate)
	mux.HandleFunc("/api/forgot-password", controller.ForgotPassword)
	mux.HandleFunc("/api/reset-password", controller.ResetPassword)

	return mux
}
