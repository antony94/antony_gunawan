package util

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/smtp"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

type response struct {
	Code    int         `json:"code"`
	Err     bool        `json:"error"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

//CreateResponse create RestAPI Response based on data passed
func CreateResponse(code int, err bool, message string, data interface{}) ([]byte, error) {
	res := response{
		code,
		err,
		message,
		data,
	}
	json, e := json.Marshal(res)
	return json, e
}

//CreateResponseNotAllowed create RestAPI Response if passed method is not matched
func CreateResponseNotAllowed() []byte {
	res := response{
		405,
		true,
		"Method Not Allowed",
		nil,
	}
	json, _ := json.Marshal(res)
	return json
}

//CreateResponseNotFound create RestAPI Response if requested url not found
func CreateResponseNotFound() []byte {
	res := response{
		404,
		true,
		"Requested API Not Found",
		nil,
	}
	json, _ := json.Marshal(res)
	return json
}

//CheckRequiredField check required field, if found empty return false else true
func CheckRequiredField(fields map[string]string) (string, bool) {
	var msg string
	var ok = true
	var keys []string
	for k, v := range fields {
		if v == "" {
			keys = append(keys, k)
			ok = false
		}
	}
	msg = strings.Join(keys, ",") + " can't be empty"
	return msg, ok
}

//GenerateBcrypt used for encrypting password
func GenerateBcrypt(s string) string {
	bs, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	return string(bs)
}

//GenerateHmacToken used for generating user API token
func GenerateHmacToken(s string) string {
	h := hmac.New(sha256.New, []byte("golang test Antony"))
	io.WriteString(h, s)
	return fmt.Sprintf("%x", h.Sum(nil))
}

//CompareBcrypt used for checking encrypted password & password
func CompareBcrypt(s1 string, hashPass string) bool {
	if bcrypt.CompareHashAndPassword([]byte(hashPass), []byte(s1)) == nil {
		return true
	}
	return false
}

//SendEmail used for sending email by declaring recipient, title and message of email
func SendEmail(rcp, title, msg string) error {
	to := make([]string, 1)
	to[0] = rcp
	auth := smtp.CRAMMD5Auth("antony@gunawan.com", "ant0ny")
	subject := "Subject: " + title + "\n"
	err := smtp.SendMail("mail.smtp2go.com:2525", auth, "antony@gunawan.com", to, []byte(subject+msg))
	if err != nil {
		log.Fatal(err)
	}
	return err
}
