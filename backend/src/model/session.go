package model

import (
	"data"
	"log"
	"strconv"
)

const dbTableSession = "sessions"

//Session struct
type Session struct {
	ID        int `json:"id"`
	uid       string
	Token     string `json:"Token"`
	createdAt string
	updatedAt string
}

//GetUIDByToken get user id by token
func GetUIDByToken(token string) (int, error) {
	var uid int
	sql := "SELECT uid FROM " + dbTableSession + " WHERE token = '" + token + "'"
	err := data.ExecuteQueryRow(sql).Scan(&uid)
	return uid, err
}

//InsertSession insert new session
func InsertSession(uid int, token string) int64 {
	sql := "INSERT INTO " + dbTableSession + " (uid, token, created_at) VALUES (" + strconv.Itoa(uid) + ", '" + token + "', NOW())"
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

//IsSessionExpired check if current session is already expired, expired if session created at more than 1 day
func IsSessionExpired(token string) bool {
	var dateDiff int
	sql := "SELECT DATEDIFF(NOW(), created_at) as date_diff FROM " + dbTableSession + " WHERE token = '" + token + "'"
	if err := data.ExecuteQueryRow(sql).Scan(&dateDiff); err != nil {
		log.Fatal(err)
	}
	return dateDiff > 0
}
