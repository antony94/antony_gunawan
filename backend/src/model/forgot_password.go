package model

import (
	"data"
	"log"
)

const dbTableForgotPassword = "forgot_passwords"

//GetUIDByForgotPassToken get user id by provided forgot password token
func GetUIDByForgotPassToken(token string) (string, error) {
	var uid string
	sql := "SELECT uid FROM " + dbTableForgotPassword + " WHERE token = '" + token + "' AND status = 0"
	err := data.ExecuteQueryRow(sql).Scan(&uid)
	return uid, err
}

//InsertForgotPassword insert new forgot password token related to user id to database
func InsertForgotPassword(uid string, token string, status string) int64 {
	sql := "INSERT INTO " + dbTableForgotPassword + " (uid, token, status, created_at) VALUES (" + uid + ", '" + token + "', " + status + ", NOW())"
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

//UpdateForgotPasswordStatusByToken update forgot password status by its token
func UpdateForgotPasswordStatusByToken(token string, status string) int64 {
	sql := "UPDATE " + dbTableForgotPassword + " SET status = " + status + " WHERE token = '" + token + "'"
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}
