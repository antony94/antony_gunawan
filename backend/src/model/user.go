package model

import (
	"data"
	"database/sql"
	"log"

	"golang.org/x/crypto/bcrypt"
)

const dbTableUser = "users"

//User struct
type User struct {
	ID        int    `json:"id"`
	Email     string `json:"email"`
	password  string
	GoogleID  string `json:"google_id"`
	Name      string `json:"name"`
	Address   string `json:"address"`
	Phone     string `json:"phone"`
	createdAt string
	updatedAt string
}

//GetUserByID get pointer to a user struct by id
func GetUserByID(id string) (*User, error) {
	var u User
	sql := "SELECT * FROM " + dbTableUser + " WHERE id = " + id
	err := data.ExecuteQueryRow(sql).Scan(&u.ID, &u.Email, &u.password, &u.GoogleID, &u.Name,
		&u.Address, &u.Phone, &u.createdAt, &u.updatedAt)
	return &u, err
}

//GetUserByEmail get pointer to a user struct by id
func GetUserByEmail(email string) (*User, error) {
	var u User
	sql := "SELECT * FROM " + dbTableUser + " WHERE email = '" + email + "'"
	err := data.ExecuteQueryRow(sql).Scan(&u.ID, &u.Email, &u.password, &u.GoogleID, &u.Name,
		&u.Address, &u.Phone, &u.createdAt, &u.updatedAt)
	return &u, err
}

//GetUserByCredential get pointer to a user struct by user credential
func GetUserByCredential(email string, password string, googleID string) (*User, error) {
	var u User
	sql := "SELECT * FROM " + dbTableUser + " WHERE (email = '" + email + "' AND password = '" + password + "') OR (email = '" + email + "' AND google_id = '" + googleID + "')"
	err := data.ExecuteQueryRow(sql).Scan(&u.ID, &u.Email, &u.password, &u.GoogleID, &u.Name,
		&u.Address, &u.Phone, &u.createdAt, &u.updatedAt)
	return &u, err
}

//InsertUser insert new user to database
func InsertUser(email string, password string, googleID string, name string, address string, phone string) int64 {
	sql := "INSERT INTO " + dbTableUser + " (email, password, google_id, name, address, phone, created_at) VALUES ('" + email + "', '" + password + "', '" + googleID + "', '" + name + "', '" + address + "', '" + phone + "', NOW())"
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

//UpdateUserByID update user fields by ID
func UpdateUserByID(id string, name string, address string, phone string) int64 {
	sql := "UPDATE " + dbTableUser + " SET name = '" + name + "' ,address = '" + address + "' ,phone='" + phone + "' WHERE id = " + id
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

//UpdateUserPasswordByID update user password fields by ID
func UpdateUserPasswordByID(id string, password string) int64 {
	sql := "UPDATE " + dbTableUser + " SET password = '" + password + "' WHERE id = " + id
	n, err := data.ExecuteStatement(sql)
	if err != nil {
		log.Fatal(err)
	}
	return n
}

//CheckUserAccountCredential used for checking if email and password that user send is correct
func CheckUserAccountCredential(email string, password string, googleID string) bool {
	u, _ := GetUserByEmail(email)
	if err := bcrypt.CompareHashAndPassword([]byte(u.password), []byte(password)); err == nil {
		return true
	}

	var s sql.NullString
	sql := "SELECT email FROM " + dbTableUser + " WHERE (email = '" + email + "' AND password = '" + password + "') OR (email = '" + email + "' AND google_id = '" + googleID + "')"
	if err := data.ExecuteQueryRow(sql).Scan(&s); err == nil {
		return true
	}
	return false
}

//IsEmailAlreadyRegistered used for checking if email already registered
func IsEmailAlreadyRegistered(email string) bool {
	var s sql.NullString
	sql := "SELECT email FROM " + dbTableUser + " WHERE email = '" + email + "'"
	if err := data.ExecuteQueryRow(sql).Scan(&s); err == nil {
		return true
	}
	return false
}
