package controller

import (
	"html/template"
	"log"
	"myapp/data"
	"myapp/endpoint"
	"net/http"
)

//file for organizing our web apps logic function
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("./src/myapp/templates/*.html"))
}

//Index handle logic for mux index
func Index(w http.ResponseWriter, r *http.Request) {
	if data.IsLoggedOn(w, r) {
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
	} else {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
	}
}

//Login handle logic for mux login
func Login(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		data := struct {
			LoginApiUrl string
		}{
			endpoint.Login,
		}
		err := tpl.ExecuteTemplate(w, "login.html", data)
		if err != nil {
			log.Fatal(err)
		}
		break
	case http.MethodPost:
		data.SetCookie(w, "user", "logged in")
		http.Redirect(w, r, "/profile", http.StatusSeeOther)
	}
}

//Register handle logic for mux register
func Register(w http.ResponseWriter, r *http.Request) {
	data := struct {
		RegisterApiUrl string
	}{
		endpoint.Register,
	}
	err := tpl.ExecuteTemplate(w, "register.html", data)
	if err != nil {
		log.Fatal(err)
	}
}

//Profile handle logic for mux profile
func Profile(w http.ResponseWriter, r *http.Request) {
	if data.IsLoggedOn(w, r) {
		data := struct {
			ProfileApiUrl       string
			ProfileUpdateApiUrl string
		}{
			endpoint.Profile,
			endpoint.ProfileUpdate,
		}
		err := tpl.ExecuteTemplate(w, "profile.html", data)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
}

//ForgotPassword handle logic for mux forgot password
func ForgotPassword(w http.ResponseWriter, r *http.Request) {
	data := struct {
		ForgotPasswordApiUrl string
	}{
		endpoint.ForgotPassword,
	}
	err := tpl.ExecuteTemplate(w, "forgot_password.html", data)
	if err != nil {
		log.Fatal(err)
	}
}

//ResetPassword handle logic for mux forgot password
func ResetPassword(w http.ResponseWriter, r *http.Request) {
	token := r.FormValue("token")
	if token == "" {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	data := struct {
		ResetPasswordApiUrl string
		Token               string
	}{
		endpoint.ResetPassword,
		token,
	}
	err := tpl.ExecuteTemplate(w, "reset_password.html", data)
	if err != nil {
		log.Fatal(err)
	}
}
