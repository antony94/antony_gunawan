package mux

//file for organizing our web apps route

import (
	"myapp/controller"
	"net/http"
)

//CreateMux create custom mux with our custom route
func CreateMux() *http.ServeMux {
	mux := http.NewServeMux()

	//create file server for serving folder in assets
	mux.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))

	//handling favicon.ico
	mux.Handle("/favicon.ico", http.NotFoundHandler())

	//handling our custom routes
	mux.HandleFunc("/", controller.Index)
	mux.HandleFunc("/login", controller.Login)
	mux.HandleFunc("/register", controller.Register)
	mux.HandleFunc("/profile", controller.Profile)
	mux.HandleFunc("/forgot-password", controller.ForgotPassword)
	mux.HandleFunc("/reset-password", controller.ResetPassword)

	return mux
}
