package endpoint

//file endpoint for organizing endpoint used for our web apps

//BaseURL url
const BaseURL = "http://app-1be35ffa-c7ed-4fee-a06d-b9b368887858.cleverapps.io/api/"

//Login url
const Login = BaseURL + "login"

//Register url
const Register = BaseURL + "register"

//Profile url
const Profile = BaseURL + "profile"

//ProfileUpdate url
const ProfileUpdate = BaseURL + "profile/update"

//ForgotPassword url
const ForgotPassword = BaseURL + "forgot-password"

//ResetPassword url
const ResetPassword = BaseURL + "reset-password"
