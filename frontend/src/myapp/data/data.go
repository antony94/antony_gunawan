package data

import (
	"net/http"
)

//file for organizing our web apps data fetching function

//SetCookie creates cookie based on name and value that passed on
func SetCookie(w http.ResponseWriter, name string, value string) {
	http.SetCookie(w, &http.Cookie{
		Name:  name,
		Value: value,
	})
}

//GetCookie get cookie based on name that passed on
func GetCookie(w http.ResponseWriter, r *http.Request, name string) *http.Cookie {
	c, err := r.Cookie(name)
	if err != nil {
		return nil
	}
	return c
}

//IsLoggedOn check if the user already logged on or not
func IsLoggedOn(w http.ResponseWriter, r *http.Request) bool {
	if GetCookie(w, r, "token") != nil {
		return true
	}
	return false
}
