$(document).ready(function () {
    $('.modal').modal();
    displayLoad()
    initMap()
    checkProfile()
});

function initMap(){
  $('#location-picker').locationpicker({
        location: {
            latitude: -6.2269108,
            longitude: 106.8086832
        },
        zoom: 11,
        radius: 0,
        inputBinding: {
            locationNameInput: $('#div-fill-profile #address')
        },
        enableAutocomplete: true,
        enableReverseGeocode: true,
    });
    $('#location-picker').locationpicker('autosize')
}

function checkProfile() {
    var url = document.querySelector('#profile-api-url').value + "?token=" + getCookie("token")
    $.get(url,
        function (data, status) {
            hideLoad()
            var user = data.data.user
            if (user.address == "" || user.name == "" || user.phone == "") displayFillProfile(user.name, user.address, user.phone)
            else displayProfile(user.email, user.name, user.address, user.phone)
        })
}

function updateProfile() {
    var divFillProfile = document.querySelector("#div-fill-profile")

    var txtName = divFillProfile.querySelector("#name")
    var txtAddress = divFillProfile.querySelector("#address")
    var txtPhone = divFillProfile.querySelector("#phone")
    
    var url = document.querySelector('#profile-update-api-url').value
    var data = {
        token: getCookie("token"),
        name: txtName.value,
        address: txtAddress.value,
        phone: txtPhone.value,
    }

    $.post(url, data,
        function(data, status){
            if (data.code <= 200 && data.code < 300) {
                updateProfileSuccess()
            } else {
                updateProfileFailed(data.message)
            }
        })
}

function updateProfileSuccess(){
    displayLoad()
    checkProfile()
}

function updateProfileFailed(msg){
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
}

function logout(){
    localStorage.clear()
    sessionStorage.clear()
    deleteCookie("token")
    location.href = "/"
}

function displayLoad() {
    $('#modal-load').modal('open')
    hideProfile()
    hideFillProfile()
}

function hideLoad() {
    $('#modal-load').modal('close')
}

function displayProfile(email, name, address, phone) {
    var divProfile = document.querySelector("#div-profile")

    var txtEmail = divProfile.querySelector("#email")
    var txtName = divProfile.querySelector("#name")
    var txtAddress = divProfile.querySelector("#address")
    var txtPhone = divProfile.querySelector("#phone")

    txtEmail.value = email
    txtName.value = name
    txtAddress.value = address
    txtPhone.value = phone

    Materialize.updateTextFields();
    divProfile.style.display = 'block'
}

function hideProfile() {
    var divProfile = document.querySelector("#div-profile")
    divProfile.style.display = 'none'
}

function displayFillProfile(name, address, phone) {
    var divFillProfile = document.querySelector("#div-fill-profile")

    var txtName = divFillProfile.querySelector("#name")
    var txtAddress = divFillProfile.querySelector("#address")
    var txtPhone = divFillProfile.querySelector("#phone")

    txtName.value = name
    txtAddress.value = address
    txtPhone.value = phone

    Materialize.updateTextFields();
    divFillProfile.style.display = 'block'
    setTimeout(function(){
        $('#location-picker').locationpicker('autosize')
    },1000)
}

function hideFillProfile() {
    var divProfile = document.querySelector("#div-fill-profile")
    divProfile.style.display = 'none'
}