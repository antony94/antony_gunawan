$(document).ready(function () {
    $('.modal').modal();
    setTimeout(function () {
        document.querySelector(".abcRioButton").style.width = "100%"
    }, 1000)
});

function register() {
    displayLoad("Registering...")
    var url = document.querySelector("#register-api-url").value;

    var email = document.querySelector("#email").value;
    var password = document.querySelector("#password").value;

    var data = {
        email: email,
        password: password,
    }

    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                registerSuccess(data.data)
            } else {
                registerFailed(data.message)
            }
        });
}

function successGoogleSignIn(user) {
    gapi.auth2.getAuthInstance().signOut();
    displayLoad("Registering...")
    var url = document.querySelector("#register-api-url").value;
    
    var profile = user.getBasicProfile();
    var data = {
        email: profile.getEmail(),
        google_id: profile.getId(),
    }

    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                registerSuccess(data.data)
            } else {
                registerFailed(data.message)
            }
        });
}

function failureGoogleSignIn(error) {
    gapi.auth2.getAuthInstance().signOut();
    $('#txt-error').html(error)
    $('#modal-error').modal('open')
}

function registerSuccess(data) {
    var token = data.token

    localStorage.clear()
    sessionStorage.clear();
    deleteCookie("token");

    setCookie("token", token, 1)
    localStorage.setItem("token", token)

    location.href = "/profile"
}

function registerFailed(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
}

function displayLoad(msg) {
    $('#txt-load').html(msg)
    $('#modal-load').modal('open')
}

function hideLoad() {
    $('#modal-load').modal('close')
}