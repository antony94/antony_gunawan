$(document).ready(function () {
    $('.modal').modal();
    setTimeout(function () {
        document.querySelector(".abcRioButton").style.width = "100%"
    }, 1000)
});

function login() {
    displayLoad("Login in...")

    var url = document.querySelector("#login-api-url").value;

    var email = document.querySelector("#email").value;
    var password = document.querySelector("#password").value;

    var data = {
        email: email,
        password: password
    }
    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                loginSuccess(data.data)
            } else {
                loginFailed(data.message)
            }
        });
}

function successGoogleSignIn(user) {
    gapi.auth2.getAuthInstance().signOut();
    displayLoad("Login in...")
    var url = document.querySelector("#login-api-url").value;
    
    var profile = user.getBasicProfile();
    var data = {
        email: profile.getEmail(),
        google_id: profile.getId(),
    }

    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                loginSuccess(data.data)
            } else {
                loginFailed(data.message)
            }
        });
}

function failureGoogleSignIn(error) {
    gapi.auth2.getAuthInstance().signOut();
    $('#txt-error').html(error)
    $('#modal-error').modal('open')
}

function loginSuccess(data) {
    var rm = document.querySelector('#remember-me').checked
    var token = data.token

    localStorage.clear()
    sessionStorage.clear();
    deleteCookie("token");
    setCookie("token", token, 1)


    if (rm) localStorage.setItem("token", token)
    else sessionStorage.setItem("token", token)
    location.href = "/profile"
}

function loginFailed(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
}

function displayLoad(msg) {
    $('#txt-load').html(msg)
    $('#modal-load').modal('open')
}

function hideLoad() {
    $('#modal-load').modal('close')
}