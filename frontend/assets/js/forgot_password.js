$(document).ready(function () {
    $('.modal').modal();
});

function forgotPassword() {
    displayLoad("Checking Email...")

    var url = document.querySelector("#forgot-password-api-url").value;

    var email = document.querySelector("#email").value;

    var data = {
        email: email,
    }
    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                forgotPasswordSuccess(data.message)
            } else {
                forgotPasswordFailed(data.message)
            }
        });
}

function forgotPasswordSuccess(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
    disabledBtnResetPassword()
}

function forgotPasswordFailed(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
}

function displayLoad(msg) {
    $('#txt-load').html(msg)
    $('#modal-load').modal('open')
}

function hideLoad() {
    $('#modal-load').modal('close')
}

function enabledBtnResetPassword(){
    var btnResetPassword = document.querySelector('#btn-reset-password');
    btnResetPassword.disabled = false
}

function disabledBtnResetPassword(){
    var btnResetPassword = document.querySelector('#btn-reset-password');
    var txtTimer = document.querySelector('#txt-timer');
    
    txtTimer.style.visibility = "visible";
    btnResetPassword.disabled = true

    var countdown = 30;
    var ivalUpdateTimer = setInterval(function(){
        countdown--;
        txtTimer.innerHTML = "Re-send after " + countdown + " seconds..."
    },1000)

    setTimeout(function(){
        clearInterval(ivalUpdateTimer)
        txtTimer.style.visibility = "hidden";
        enabledBtnResetPassword()
    },countdown * 1000)
}