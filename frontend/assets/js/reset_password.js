$(document).ready(function () {
    $('.modal').modal();
});

function resetPassword() {
    displayLoad("Updating Password...")

    var url = document.querySelector("#reset-password-api-url").value;

    var token = document.querySelector("#token").value;
    var password = document.querySelector("#password").value;

    var data = {
        token: token,
        password: password,
    }
    $.post(url
        , data
        , function (data, status) {
            hideLoad()
            if (data.code <= 200 && data.code < 300) {
                resetPasswordSuccess(data.message)
            } else {
                resetPasswordFailed(data.message)
            }
        });
}

function resetPasswordSuccess(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
    location.href = "/login"
}

function resetPasswordFailed(msg) {
    $('#txt-error').html(msg)
    $('#modal-error').modal('open')
}

function displayLoad(msg) {
    $('#txt-load').html(msg)
    $('#modal-load').modal('open')
}

function hideLoad() {
    $('#modal-load').modal('close')
}